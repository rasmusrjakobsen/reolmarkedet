namespace RackMarket;
public class Controller
{
    public static List<SalesRack> SalesRackList = new List<SalesRack>();
    public static List<SalesRackRenter> SalesRackRenterList = new List<SalesRackRenter>();
    public static List<Booking> BookingList = new List<Booking>();
    public static void AddToList(SalesRackRenter salesRackRenter)
    {
        SalesRackRenterList.Add(salesRackRenter);
    }
    public static void AddToList(SalesRack salesRack)
    {
        SalesRackList.Add(salesRack);
    }
    public static void AddToList(Booking booking)
    {
        BookingList.Add(booking);
    }




    
}