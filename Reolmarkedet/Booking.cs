namespace RackMarket;
public class Booking
{
    public int BookingId {get; set;}
    public int SalesRackId {get; set;}
    public DateTime StartDate {get; set;}
    public DateTime EndDate {get; set;}
    public Booking(int salesRackId, DateTime startDate, DateTime endDate)
    {
        BookingId=Controller.BookingList.Count + 1;
        SalesRackId=salesRackId;
        StartDate=startDate;
        EndDate=endDate;
    }
    
}