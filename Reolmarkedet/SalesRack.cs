namespace RackMarket;
public class SalesRack
{
    public int SalesRackId {get; set;}
    public string Type {get; set;}
    public bool AvailabilityStatus {get; set;}

    public SalesRack()
    {
        SalesRackId=Controller.SalesRackList.Count + 1;
        Type="Standard";
        AvailabilityStatus=true;
    }
    public static string SalesRackToAvailable(SalesRack salesRack)
    {
        if(salesRack.AvailabilityStatus==false)
        {
            salesRack.AvailabilityStatus=true;
            return $"{salesRack.SalesRackId} status er nu sat som ledig";
        }
        return $"{salesRack.SalesRackId} status var ledig, ingen ændring foretaget";
    }
    // public int GetTotalNumberOfAvailableSalesRacks()
    // {
    //     int numOfAvailableSalesRacks = 0;
    //     foreach(SalesRack SalesRack in SalesRackList)
    //     {
    //         if(SalesRack.AvailabilityStatus == true)
    //             numOfAvailableSalesRacks++;
    //     }
    //     return numOfAvailableSalesRacks;
    // }
    // public int GetNumberOfAvailableSalesRacksOfType(string type)
    // {
    //     int numOfAvailableSalesRacksOfType=0;
    //     foreach(SalesRack SalesRack in SalesRackList)
    //     {
    //         if(SalesRack.AvailabilityStatus==true && SalesRack.Type == type)
    //             numOfAvailableSalesRacksOfType++;
    //     }
    //     return numOfAvailableSalesRacksOfType;
    // }
    


}