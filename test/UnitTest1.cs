// namespace RackMarket;

// [TestClass]
// public class UnitTest1
// {
//     SalesRack s1, s2, s3;
//     SalesRackRenter a1, a2, a3;
//     [TestInitialize]
//     public void init()
//     {
//         s1 = new SalesRack(1, "Standard", true);
//         s2 = new SalesRack(2, "Standard", true);
//         s3 = new SalesRack(3, "GlassShelf", true);

//         a1 = new SalesRackRenter("Rasmus", "rasmus@ucl.dk", 26447517);
//         a2 = new SalesRackRenter("Rasmusa", "rasmusa@ucl.dk", 26447518);
//         a3 = new SalesRackRenter("Rasmusaa", "rasmusaa@ucl.dk", 26447519);

//         Controller.AddToList(s1);
//         Controller.AddToList(s2);
//         Controller.AddToList(s3);
//         Controller.AddToList(a1);
//         Controller.AddToList(a2);
//         Controller.AddToList(a3);
//     }
//     [TestMethod]
//     public void TestBookAStorage()
//     {
//         Assert.AreEqual("Den ønskede tid er ledig, booking er godkendt! Du har reol 1, skal møde op d. 1 og prisen for 1 er 350", SalesRackRenter.BookAStorage(a1, 1, 1));
//     }
//     [TestMethod]
//     public void TestSalesRackToAvailable()
//     {
//         Assert.AreEqual($"2 status var ledig, ingen ændring foretaget", SalesRack.SalesRackToAvailable(s2));
//     }
//     [TestMethod]
//     public void TestSalesRackAddToList()
//     {
//         Assert.AreEqual("Standard", Controller.SalesRackList[0].Type);
//     }

// }